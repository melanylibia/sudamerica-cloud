#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import path
from sqlite3 import connect
from PIL import Image
import os
import numpy as np
import matplotlib.pyplot as plt
from pymongo import MongoClient,DESCENDING
from wordcloud import WordCloud
from stop_words import get_stop_words
import json
import time 


start_time = time.time()
colors = []
#base_path = "/var/www/html/esperanza/public/img/home"
base_path = ""

def grey_color_func(word, font_size, position,orientation,random_state=None, **kwargs):
    return(colors[np.random.randint(0,len(colors))])

def get_word_image(text,stopwords,country):
    try:
        mask = np.array(Image.open(path.join(d, base_path+"sudamerica/"+country+"_mask.png")))
        stopwords = set(stop_words)
        wc = WordCloud(background_color="white", max_words=1500, mask=mask,
                    stopwords=stopwords, contour_width=2, contour_color='rgb(73, 70, 108)')
        wc.generate(text)
        wc.recolor(color_func = grey_color_func)
        wc.to_file(path.join(d, base_path+"sudamerica_word/"+country+"_words.png"))
    except:
        pass
