# Nube de Palabras (Paises)
![](https://raw.githubusercontent.com/cr0wg4n/sudamerica-cloud/master/sudamerica_word/bolivia_words.png)
## Instalación

```bash
pip3 install -r requirements.txt
```

__Docker__

build a docker container

```
docker-compose up 
```

**Important** cloudwords:0.1 the app will terminate first but created the image(png) in sudamerica_word folder.

__next_step__ gitlab and ci/cd